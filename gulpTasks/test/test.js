import gulp from 'gulp';
import runSequence from 'run-sequence';

const options = {
  name: 'test'
};

gulp.task(options.name, (done) => {
  runSequence('stylint', 'puglint', done);
});
