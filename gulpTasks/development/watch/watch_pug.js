import gulp from 'gulp';
const browserSync = require('browser-sync').create();

const options = {
  src: [
    './src/pug/**/*.pug'
  ],
  dependencies: [
    'build:html'
  ],
  name: 'watch:pug'
};

gulp.task(options.name, () => {
  gulp
    .watch(options.src, options.dependencies, browserSync.reload);
});
