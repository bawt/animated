import gulp from 'gulp';
import runSequence from 'run-sequence';
const browserSync = require('browser-sync').create();

const options = {
  name: {
    watch: 'watch',
    browser: 'serve'
  }
};

gulp.task(options.name.browser, () => {
  browserSync.init({
    server: {
      baseDir: './dev/html/'
    }
  });
});

gulp.task(options.name.watch, (done) => {
  runSequence('watch:css', 'watch:pug', done);
});
