import gulp from 'gulp';
const browserSync = require('browser-sync').create();

const options = {
  src: [
    './src/styl/**/*.styl'
  ],
  dependencies: ['build:css:dev'],
  name: 'watch:css'
};

gulp.task(options.name, () => {
  return gulp
    .watch(options.src, options.dependencies, browserSync.reload);
});
