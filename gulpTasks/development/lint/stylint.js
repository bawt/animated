import gulp from 'gulp';
import stylint from 'gulp-stylint';

const options = {
  src: [
    './src/styl/**/*.styl'
  ],
  name: 'stylint'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(stylint())
    .pipe(stylint.reporter(require('stylint-stylish')))
    .pipe(stylint.reporter('fail', { failOnWarning: true }));
});
