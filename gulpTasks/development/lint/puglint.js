import gulp from 'gulp';
import puglint from 'gulp-pug-lint';

const options = {
  src: [
    './src/pug/**/*.pug'
  ],
  name: 'puglint'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(puglint());
});
