import gulp from 'gulp';
import concat from 'gulp-concat';

const options = {
  src: [
    './dev/css/**/*.css'
  ],
  dest: './dist/css',
  name: 'concat:css',
  concat: 'animated.css'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(concat(options.concat))
    .pipe(gulp.dest('./dev/html/'))
    .pipe(gulp.dest(options.dest));
});
