import gulp from 'gulp';
import inject from 'gulp-inject';

const options = {
  target: [
    './dev/html/index.html'
  ],
  src: [
    './dev/html/animated.css'
  ],
  name: 'inject:css',
  dest: './dev/html'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.target)
    .pipe(inject(gulp.src(options.src, {
      read: false
    }), {
        relative: true
      }))
    .pipe(gulp.dest(options.dest));
})
