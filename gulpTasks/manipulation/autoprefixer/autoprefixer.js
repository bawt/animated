import gulp from 'gulp';
import autoprefixer from 'gulp-autoprefixer';

const options = {
  src: [
    './dist/css/animated.css'
  ],
  dest: './dist/css/',
  name: 'autoprefixer'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest(options.dest));
});
