import gulp from 'gulp';
import postcss from 'gulp-postcss';
import rename from 'gulp-rename';

const options = {
  src: [
    './dist/css/animated.css'
  ],
  dest: './dist/css/',
  name: 'min:css',
  suffix: '.min'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(postcss([
      require('postcss-clean')
    ]))
    .pipe(rename({
      suffix: options.suffix
    }))
    .pipe(gulp.dest(options.dest));
});
