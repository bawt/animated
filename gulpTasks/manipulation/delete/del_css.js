import gulp from 'gulp';
import clean from 'gulp-clean';

const options = {
  src: [
    './dev/css'
  ],
  name: 'del:css'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src, {
      read: false
    })
    .pipe(clean());
});
