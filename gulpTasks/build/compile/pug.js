import gulp from 'gulp';
import pug from 'gulp-pug';

const options = {
  src: [
    './src/pug/**/*.pug'
  ],
  dest: './dev/html/',
  name: 'pug'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(options.dest));
});
