import gulp from 'gulp';
import stylus from 'gulp-stylus';
import nib from 'nib';

const options = {
  src: [
    './src/styl/**/*.styl'
  ],
  dest: './dev/css/',
  name: 'stylus'
};

gulp.task(options.name, () => {
  return gulp
    .src(options.src)
    .pipe(stylus({
      use: nib()
    }))
    .pipe(gulp.dest(options.dest));
});
