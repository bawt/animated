import gulp from 'gulp';
import runSequence from 'run-sequence';

const options = {
  name: 'build:dev'
};

gulp.task(options.name, (done) => {
  runSequence('stylint', 'puglint', 'build:css:dev', 'pug', done);
});
