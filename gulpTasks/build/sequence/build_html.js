import gulp from 'gulp';
import runSequence from 'run-sequence';

const options = {
  name: 'build:html'
};

gulp.task(options.name, (done) => {
  runSequence('puglint', 'pug', 'inject:css', done);
});

