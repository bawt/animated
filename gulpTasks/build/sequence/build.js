import gulp from 'gulp';
import runSequence from 'run-sequence';

const options = {
  name: 'build'
};

gulp.task(options.name, (done) => {
  runSequence('build:css:dist', done);
});
