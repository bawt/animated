import gulp from 'gulp';
import runSequence from 'run-sequence';

const options = {
  name: {
    dist: 'build:css:dist',
    dev: 'build:css:dev'
  }
};

gulp.task(options.name.dist, (done) => {
  runSequence('stylus', 'concat:css', 'min:css', 'del:css', done);
});

gulp.task(options.name.dev, (done) => {
  runSequence('stylint', 'stylus', 'concat:css', 'min:css', done);
});
