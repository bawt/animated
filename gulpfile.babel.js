import requireDir from 'require-dir';
import gulp from 'gulp';

requireDir('./gulpTasks', { recurse: true });

gulp.task('default', ['stylus']);
