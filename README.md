# css-animated

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![devDependency Status](https://david-dm.org/bawt/animated/dev-status.svg)](https://david-dm.org/bawt/animated#info=devDependencies)
[![ReviewNinja](https://app.review.ninja/54779798/badge)](https://app.review.ninja/bawt/animated)
[![Build Status](https://travis-ci.org/bawt/animated.svg?branch=master)](https://travis-ci.org/bawt/animated)
a cross-browser css animation library
